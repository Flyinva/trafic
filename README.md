# utilisation des données de trafic de Rennes Métropole

## Principes généraux

Rennes Métropole publie en données ouvertes [l'état du trafic en temps réel](https://data.rennesmetropole.fr/explore/dataset/etat-du-trafic-en-temps-reel/information/) sur divers tronçons. Ces données incluent la vitesse moyenne sur le tronçon et une indication de la fluidité de la circulation  par tranche de 3 minutes.

L'idée vient de [ce Tweet](https://twitter.com/taflevelo/status/1248190199022137345) qui montre que la vitesse moyenne de certains tronçons est bien supérieure à la vitesse autorisée. L'idée était d'ajouter la limitation de vitesse aux données. Cette information est disponible dans OpenStreetMap.

Les données sont stockées dans des bases Graphite qui ne supportent que des données temporelles numériques. Il n'est pas possible de stocker d'autres données. 3 stockages différents sont utilisés :

 - les données temporelles dans Graphite : une donnée toutes les 3 minutes (~300 Mo pour 1800 points de mesure sur 7 jours)
 - les coordonnées géographiques de chaque point de données (incluses dans les données de Rennes Métropole) sont stockés dans un fichier JSON static pour le plugin [Grafana Map Panel](https://community.panodata.org/t/grafana-map-panel/121)
 - la limitation de vitesse est extraite de l'[API Overpass turbo](https://wiki.openstreetmap.org/wiki/FR:Overpass_turbo) et stockée dans une base de données


## Fonctionnement

Le script `trafic` est lancé toute les 3 minutes. `trafic` appelle l'[API Rennes Métropole](https://data.rennesmetropole.fr/explore/dataset/etat-du-trafic-en-temps-reel/api/) et envoie les données via le [plain text protocol](https://graphite.readthedocs.io/en/latest/feeding-carbon.html#the-plaintext-protocol) à [Graphite](https://graphite.readthedocs.io/en/latest/whisper.html).

Le script `metadata` appelle l'[API de Rennes Métropole](https://data.rennesmetropole.fr/explore/dataset/etat-du-trafic-en-temps-reel/api/). Le script stocke l'identifiant du tronçon, le nom de la voie et les coordonnées géographiques dans un fichier JSON qui sera utilisé par [Grafana Map Panel](https://community.panodata.org/t/grafana-map-panel/121). Ces données sont également stockées dans une table SQL.

`metadata` appelle également l'[API Overpass turbo](https://wiki.openstreetmap.org/wiki/FR:Overpass_turbo) pour extraire le champ [maxspeed](https://wiki.openstreetmap.org/wiki/FR:Key:maxspeed) où les contributeurs stockent la vitesse maximale autorisée. La donnée est stockée dans une table SQL liée avec les données de Rennes Métropole avec le champ `predefinedLocationReference`. La limitation de vitesse issue d'OpenStreetMap peut être corrigée à la main dans la base de données. L'importation n'est pas fiable à 100% (croisement, zone dense, coordonnées parfois imprécises).

## Tableaux de bord

Les tableaux de bord sont créés dans [Grafana](https://grafana.org). Ces tableaux de bord sont stockés dans le [dépôt Git](https://framagit.org/Flyinva/trafic/-/tree/master/grafana).

