SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `trafic_osm`;
CREATE TABLE `trafic_osm` (
  `predefinedlocationreference` varchar(15) NOT NULL,
  `maxspeed_osm` tinyint(3) NOT NULL,
  `maxspeed` tinyint(3) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`predefinedlocationreference`),
  CONSTRAINT `trafic_osm_ibfk_1` FOREIGN KEY (`predefinedlocationreference`) REFERENCES `trafic_sections` (`predefinedlocationreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `trafic_sections`;
CREATE TABLE `trafic_sections` (
  `predefinedlocationreference` varchar(15) NOT NULL,
  `denomination` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `displayed` tinyint(1) unsigned DEFAULT 0,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `radius` float unsigned NOT NULL DEFAULT 10,
  `no_trafic_speed` float unsigned DEFAULT NULL,
  `comment` tinytext NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`predefinedlocationreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `trafic_speeding`;
CREATE TABLE `trafic_speeding` (
  `predefinedlocationreference` varchar(15) NOT NULL,
  `speed` smallint(3) NOT NULL,
  `time` int(11) NOT NULL,
  KEY `predefinedlocationreference` (`predefinedlocationreference`),
  CONSTRAINT `trafic_speeding_ibfk_1` FOREIGN KEY (`predefinedlocationreference`) REFERENCES `trafic_sections` (`predefinedlocationreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

