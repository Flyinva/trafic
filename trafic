#!/bin/bash

[[ "$TRACE"  ]] && set -x
[[ -f $HOME/.config/trafic/config ]] && . $HOME/.config/trafic/config

. $LIBS

get() {
  curl --silent "$API_URL"
}

format(){
  local metadata_file="$1" ; shift
  local now=$(date +%s)

  jq \
    --raw-output \
    --arg now $now \
    --arg reliability $RELIABILITY \
    --slurpfile metadata "$metadata_file" \
    --argjson status \
    "{\"unknown\":0,\"freeFlow\":1,\"heavy\":2,\"congested\":3,\"impossible\":4}" \
    ' .records[] |
      .fields |
      .predefinedlocationreference as $id |
      .averagevehiclespeed as $speed |
      ( $metadata[0][] | select( .[0] ==$id ) | .[1] | tonumber ) as $max_speed |
"trafic.\($id).reliability \(.traveltimereliability | tostring) \($now)
trafic.\($id).status \($status[.trafficstatus] | tostring) \($now)
trafic.\($id).speed \($speed | tostring) \($now)",
      (select( .traveltimereliability > ($reliability|tonumber) ) |
"trafic.\($id).diffspeed \($speed - $max_speed) \($now)")
    '
}

metadata() {
  cat <<EOT | database | jq --slurp --raw-input --compact-output 'split("\n") | map(split("\t"))'
SELECT
  ${DB_PREFIX}sections.predefinedlocationreference, maxspeed
FROM
  ${DB_PREFIX}sections, ${DB_PREFIX}osm
WHERE
  ${DB_PREFIX}sections.predefinedlocationreference = ${DB_PREFIX}osm.predefinedlocationreference
EOT
}

send(){
  nc -q0 localhost 2003
}

main (){
  local metadata="$(tempfile)"

  metadata > $metadata
  get | format $metadata | send

  rm $metadata
}

[[ "$0" == "$BASH_SOURCE" ]] && main "$@"

# vim:tabstop=2:expandtab:shiftwidth=2
